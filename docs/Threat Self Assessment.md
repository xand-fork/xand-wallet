The wallet software is run as an Electron application installed on your computer.

## Secrets

| Secret | Usage | Storage | Encrypted at Rest | Encrypted in Transit
| -- | -- | -- | -- | -- 
| Member API JWT Token | allows access to the Member API from the wallet | localstorage | no | no 