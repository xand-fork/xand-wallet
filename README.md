# XAND Wallet

[![coverage report](https://gitlab.com/TransparentIncDevelopment/thermite/badges/develop/coverage.svg)](https://gitlab.com/TransparentIncDevelopment/thermite/commits/develop)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

## Prereqs
Install [yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable) package manager.
 
## Building and Running
```console
yarn install                        # Install any dependencies
yarn run electron:generate-icons    # Generate icon sets from public/icon.png
yarn launch                         # Compiles and hot-reloads for development
yarn build                          # Build production version of wallet without publishing packages
yarn publish                        # Build wallet and publish to Bintray
yarn test                           # Run tests
yarn cover                          # Run test coverage
yarn lint                           # Lint
yarn fix                            # Attempt to fix lint issues
yarn clean                          # Remove installed packages and generated code
```

### Running the wallet software

You can run the software from the source code by using the `yarn launch` command in the xand-wallet directory. This will launch a development version of the wallet.

If you'd like to install a packaged version of the wallet, you'll need to download it from our [Artifactory](https://transparentinc.jfrog.io/) repository:
1. From the `Artifacts` section, navigate to the `artifacts-external` directory. All wallet versions and platforms are in the `xand-wallet` subdirectory.
2. Choose the version you'd like to install (i.e. `4.0.1`) and download the package corresponding to your OS (win, linux, or mac).
3. If you are using the Windows version of the wallet you can just double click to run the .exe file once it's downloaded. For the mac and linux .zip files, you'll need to first unzip the file. You can then run the wallet by double-clicking the `xand-wallet` package in the unzipped directory.

## Publishing
The wallet is built using [electron-builder](https://www.electron.build/) and gets published to [Artifactory](https://transparentinc.jfrog.io/).

To manually publish a new wallet version you can run the `publish-beta-wallet` jobs. This will publish a beta version and upload it to our Artifactory repository.

Whenever new code is merged into the master branch a new release version is automatically published.

## Configuration

### API URL
To set the API target, edit it in the wallet UI:

1. Click on the user icon
1. Type in the URL under Member API URL
 * For local development:  `localhost` 
 * For an environment, indicate the url:, e.g.: `https://member-api.dev.xand.tools`
 
 > Note, use the full header (e.g. "https")

![alt text](edit_wallet_URL.png "Editing Wallet URL in the UI")

See [Configuration Reference](https://cli.vuejs.org/config/) for Vue configuration information.

### Contacts

When adding a contact to the wallet, a contacts file is written to the wallet's OS-specific userdata folder, 
set by [Electron](https://github.com/electron/electron/blob/master/docs/api/app.md#appgetpathname)

The file is appended with the wallet's public key.

E.g. (Linux):
`~/.config/xand-wallet/contact-<public key>`

