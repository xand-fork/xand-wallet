import "reflect-metadata";
import { expect } from "chai";
import InMemoryStorage from "../../mocks/storage/InMemoryStorage";
import UserSettingsStorage from "@/persistence/UserSettingsStorage";

describe("MRUBankAccountStorage", () => {
  const firstAddress = "some-address-value";
  const secondAddress = "a-second-address-value";
  let userSettingsStorage: UserSettingsStorage;

  beforeEach(() => {
    userSettingsStorage = new UserSettingsStorage(new InMemoryStorage());
  });

  it("getMRUBankAccount without MRU", () => {
    const bankAccountId = userSettingsStorage.getMRUBankAccount(firstAddress);
    expect(bankAccountId).to.be.undefined;
  });

  it("getMRUBankAccount and setMRUBankAccount with MRU set for same public address", () => {
    const setAccountId = 5;
    userSettingsStorage.setMRUBankAccount(firstAddress, setAccountId);
    const bankAccountId = userSettingsStorage.getMRUBankAccount(firstAddress);
    expect(bankAccountId).to.equal(setAccountId);
  });

  it("getMRUBankAccount with MRU set for different public address", () => {
    const setAccountId = 5;
    userSettingsStorage.setMRUBankAccount(firstAddress, setAccountId);
    const bankAccountId = userSettingsStorage.getMRUBankAccount(secondAddress);
    expect(bankAccountId).to.not.equal(setAccountId);
  });

  it("getMRUBankAccount with multiple MRU set for public addresses", () => {
    const setAccountId = 5;
    const secondAccountId = 7;
    userSettingsStorage.setMRUBankAccount(firstAddress, setAccountId);
    userSettingsStorage.setMRUBankAccount(secondAddress, secondAccountId);
    let bankAccountId = userSettingsStorage.getMRUBankAccount(firstAddress);
    expect(bankAccountId).to.equal(setAccountId);
    bankAccountId = userSettingsStorage.getMRUBankAccount(secondAddress);
    expect(bankAccountId).to.equal(secondAccountId);
  });

  it("setMRUBankAccount remove item when setting undefined", () => {
    const setAccountId = 5;
    userSettingsStorage.setMRUBankAccount(firstAddress, setAccountId);
    userSettingsStorage.setMRUBankAccount(firstAddress, undefined);
    const bankAccountId = userSettingsStorage.getMRUBankAccount(firstAddress);
    expect(bankAccountId).to.be.undefined;
  });
});
