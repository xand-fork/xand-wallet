import "reflect-metadata";
import { expect } from "chai";
import InMemoryStorage from "../../mocks/storage/InMemoryStorage";

describe("InMemoryStorage", () => {
  let inMemoryStorage: InMemoryStorage;

  beforeEach(() => {
    inMemoryStorage = new InMemoryStorage();
  });

  it("getItem of non-existent key", () => {
    const retrievedValue = inMemoryStorage.getItem("non-existent-key");
    expect(retrievedValue).to.be.null;
  });

  it("setItem and getItem", () => {
    const value = "value";
    inMemoryStorage.setItem("key", value);
    const retrievedValue = inMemoryStorage.getItem("key");
    expect(retrievedValue).to.be.equal(value);
  });

  it("setItem on existing item", () => {
    const value = "value";
    inMemoryStorage.setItem("key", "overridable-value");
    inMemoryStorage.setItem("key", value);
    const retrievedValue = inMemoryStorage.getItem("key");
    expect(retrievedValue).to.be.equal(value);
  });

  it("removeItem on non-existing item", () => {
    inMemoryStorage.removeItem("key");
    const retrievedValue = inMemoryStorage.getItem("key");
    expect(retrievedValue).to.be.null;
  });

  it("removeItem on existing item", () => {
    const value = "value";
    inMemoryStorage.setItem("key", value);
    inMemoryStorage.removeItem("key");
    const retrievedValue = inMemoryStorage.getItem("key");
    expect(retrievedValue).to.be.null;
  });

  it("clear on empty memory storage", () => {
    inMemoryStorage.clear();
    expect(inMemoryStorage.length).to.be.equal(0);
  });

  it("clear on non-empty memory storage", () => {
    inMemoryStorage.setItem("key", "value");
    expect(inMemoryStorage.length).to.be.equal(1);
    inMemoryStorage.clear();
    expect(inMemoryStorage.length).to.be.equal(0);
  });

  it("key index when no values exist", () => {
    expect(inMemoryStorage.key(0)).to.be.null;
  });

  it("key index retrieval", () => {
    const value = "value";
    const keys = ["first-key", "second-key", "third-key"];
    for (const key of keys) {
      inMemoryStorage.setItem(key, value);
    }
    for (let i = 0; i < keys.length; i++) {
      expect(inMemoryStorage.key(i)).to.be.equal(keys[i]);
    }
    // One past the number of keys available.
    expect(inMemoryStorage.key(keys.length)).to.be.null;
    // Multiple past the number of keys available.
    expect(inMemoryStorage.key(keys.length + 2)).to.be.null;
  });

  it("length checks", () => {
    expect(inMemoryStorage.length).to.be.equal(0);
    inMemoryStorage.setItem("key", "value");
    expect(inMemoryStorage.length).to.be.equal(1);
    inMemoryStorage.setItem("key2", "value");
    expect(inMemoryStorage.length).to.be.equal(2);
    inMemoryStorage.setItem("key2", "value2");
    expect(inMemoryStorage.length).to.be.equal(2);
    inMemoryStorage.removeItem("key2");
    expect(inMemoryStorage.length).to.be.equal(1);
  });
});
