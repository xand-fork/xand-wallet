import { expect } from "chai";
import {
  OperationType,
  TransactionState,
  Transaction
} from "xand-member-api-client/dist";
import ExportableTransaction from "@/models/ExportableTransaction";
import moment from "moment";

describe("Given", () => {
  let record: ExportableTransaction;
  const baseOperation = {
    status: { state: TransactionState.Confirmed },
    datetime: "2020-02-05T19:00:00Z"
  };

  const memberAddress = "my address";

  // const userDetails = {
  //   registered: true,
  //   userBalanceInMinorUnit: 123.45,
  //   lastRefreshedDateTime: "dsaf",
  //   walletBalanceStatus: "some status",
  //   address: "my address"
  // };

  describe("an instance of ExportableTransaction", () => {
    before(() => {
      record = new ExportableTransaction();
      record.bankName = "Joe Exotic's Tiger Bank";
      record.routingNumber = "666";
      record.accountNumber = "9876";
      record.txnId = "1234";
      record.dateTime = "2020-02-05T11:00:00.000-08:00";
      record.txnTypeLabel = "Payment Sent";
      record.amount = "$3000.00";
      record.toAddress = "Allen";
      record.fromAddress = "Joe Exotic";
      record.state = TransactionState.Confirmed.toString();
    });

    it("toString() emits a formatted record.", () => {
      expect(record.toString()).to.equal(
        `"2020-02-05T11:00:00.000-08:00","Payment Sent","Allen","Joe Exotic","$3000.00","Joe Exotic's Tiger Bank","666","9876","1234","confirmed"`
      );
    });

    it("formats the correct header row", () => {
      expect(ExportableTransaction.header()).to.equal(
        "Date/Time,Txn Type,To,From,Txn Amount,Bank Name,Routing Number,Acct Number,Txn ID,Txn State"
      );
    });

    describe("And one of the fields has a comma in it.", () => {
      it("then the field is enclosed in quotes so that it is not misinterpreted by the csv parser.", () => {
        record.toAddress = "Wolf, Ram, & Hart";

        expect(record.toString()).to.contain(`,"Wolf, Ram, & Hart",`);
      });
    });

    describe(`And one of the fields has a " in it.`, () => {
      it("then the field is escaped so that it is not misinterpreted by the csv parser.", () => {
        record.toAddress = `Quoted " String`;

        expect(record.toString()).to.contain(`,"Quoted "" String"`);
      });
    });
  });

  describe("a Creation Request returned from the Member API", () => {
    const operation: Transaction = {
      ...baseOperation,
      bankAccount: {
        shortName: "Joe Exotic's account",
        id: 1,
        bankId: 10,
        bankName: "Joe Exotic's Tiger Bank",
        routingNumber: "666",
        maskedAccountNumber: "9876"
      },
      transactionId: "1234",
      operation: OperationType.CreationRequest,
      amountInMinorUnit: 1295,
      destinationAddress: "abc123"
    };

    let result: ExportableTransaction;
    before(() => {
      result = ExportableTransaction.fromTransaction(operation, memberAddress);
    });

    it("Then amount is formatted correctly.", () => {
      expect(result.amount).to.equal("$12.95");
    });

    it("Then datetime is localized", () => {
      expect(result.dateTime).to.equal(
        moment
          .utc("2020-02-05T11:00:00.000-08:00")
          .local()
          .toISOString(true)
      );
    });

    it("Then transaction type for the export is 'Transfer to Wallet'", () => {
      expect(result.txnTypeLabel).to.equal("Transfer to Wallet");
    });

    it("Then id is 1234", () => {
      expect(result.txnId).to.equal("1234");
    });

    it("Then from address is empty", () => {
      expect(result.fromAddress).to.equal("");
    });

    it("Then to address is empty", () => {
      expect(result.toAddress).to.equal("");
    });

    it("Then bank name is Joe Exotic's Tiger Bank", () => {
      expect(result.bankName).to.equal("Joe Exotic's Tiger Bank");
    });

    it("Then routing number matches", () => {
      expect(result.routingNumber).to.equal("666");
    });

    it("Then account number is ${maskedAccountNumber}", () => {
      expect(result.accountNumber).to.equal("9876");
    });

    it("Then state is confirmed", () => {
      expect(result.state).to.equal(TransactionState.Confirmed.toString());
    });
  });

  // Redeem conversion
  describe("a Redeem returned from the Member API", () => {
    let result: ExportableTransaction;
    before(() => {
      const operation: Transaction = {
        ...baseOperation,
        bankAccount: {
          shortName: "Joe Exotic's account",
          id: 1,
          bankId: 10,
          bankName: "Joe Exotic's Tiger Bank",
          routingNumber: "666",
          maskedAccountNumber: "9876"
        },
        transactionId: "5678",
        operation: OperationType.Redeem,
        amountInMinorUnit: 1187,
        destinationAddress: "abc123"
      };

      result = ExportableTransaction.fromTransaction(operation, memberAddress);
    });

    it("amount is formatted", () => {
      expect(result.amount).to.equal("$11.87");
    });

    it("datetime is localized", () => {
      expect(result.dateTime).to.equal(
        moment
          .utc("2020-02-05T11:00:00.000-08:00")
          .local()
          .toISOString(true)
      );
    });

    it("transaction type is formatted", () => {
      expect(result.txnTypeLabel).to.equal("Transfer to Bank");
    });

    it("id is populated", () => {
      expect(result.txnId).to.equal("5678");
    });

    it("from address is empty", () => {
      expect(result.fromAddress).to.equal("");
    });

    it("to address is empty", () => {
      expect(result.toAddress).to.equal("");
    });

    it("bank name is correct", () => {
      expect(result.bankName).to.equal("Joe Exotic's Tiger Bank");
    });

    it("routing number is populated", () => {
      expect(result.routingNumber).to.equal("666");
    });

    it("account number populated", () => {
      expect(result.accountNumber).to.equal("9876");
    });

    it("state is confirmed", () => {
      expect(result.state).to.equal(TransactionState.Confirmed.toString());
    });
  });

  // Sent payment conversion
  describe("a sent Payment returned from the Member API", () => {
    let result: ExportableTransaction;
    before(() => {
      const operation = {
        ...baseOperation,
        transactionId: "5678",
        operation: OperationType.Payment,
        amountInMinorUnit: 1187,
        signerAddress: "my address",
        destinationAddress: "other address"
      };

      result = ExportableTransaction.fromTransaction(operation, memberAddress);
    });

    it("amount is formatted", () => {
      expect(result.amount).to.equal("$11.87");
    });

    it("datetime is localized", () => {
      expect(result.dateTime).to.equal(
        moment
          .utc("2020-02-05T11:00:00.000-08:00")
          .local()
          .toISOString(true)
      );
    });

    it("transaction type is formatted", () => {
      expect(result.txnTypeLabel).to.equal("Payment Sent");
    });

    it("id is populdated", () => {
      expect(result.txnId).to.equal("5678");
    });

    it("from address is populated", () => {
      expect(result.fromAddress).to.equal("my address");
    });

    it("to address is populated", () => {
      expect(result.toAddress).to.equal("other address");
    });

    it("bank name is empty", () => {
      expect(result.bankName).to.equal("");
    });

    it("routing number is empty", () => {
      expect(result.routingNumber).to.equal("");
    });

    it("account number is empty", () => {
      expect(result.accountNumber).to.equal("");
    });

    it("state is confirmed", () => {
      expect(result.state).to.equal(TransactionState.Confirmed.toString());
    });
  });

  // Send payment conversion
  describe("a recieved payment returned from the Member API", () => {
    let result: ExportableTransaction;
    before(() => {
      const operation = {
        ...baseOperation,
        transactionId: "5678",
        operation: OperationType.Payment,
        amountInMinorUnit: 1187,
        signerAddress: "other address",
        destinationAddress: "my address"
      };

      result = ExportableTransaction.fromTransaction(operation, memberAddress);
    });

    it("amount is formatted", () => {
      expect(result.amount).to.equal("$11.87");
    });

    it("datetime is localized", () => {
      expect(result.dateTime).to.equal(
        moment
          .utc("2020-02-05T11:00:00.000-08:00")
          .local()
          .toISOString(true)
      );
    });

    it("transaction type is formatted", () => {
      expect(result.txnTypeLabel).to.equal("Payment Received");
    });

    it("id is populated", () => {
      expect(result.txnId).to.equal("5678");
    });

    it("from address is populated", () => {
      expect(result.fromAddress).to.equal("other address");
    });

    it("to address is populated", () => {
      expect(result.toAddress).to.equal("my address");
    });

    it("bank name is empty", () => {
      expect(result.bankName).to.equal("");
    });

    it("routing number is empty", () => {
      expect(result.routingNumber).to.equal("");
    });

    it("account number is empty", () => {
      expect(result.accountNumber).to.equal("");
    });

    it("state is confirmed", () => {
      expect(result.state).to.equal(TransactionState.Confirmed.toString());
    });
  });
});
