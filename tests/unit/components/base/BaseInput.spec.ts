import { shallowMount } from "@vue/test-utils";
import { expect } from "chai";

import BaseInput from "../../../../src/components/base/BaseInput.vue";

describe("BaseInput Component", () => {
  it("mounts", () => {
    const wrapper = shallowMount(BaseInput);
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(BaseInput)).to.be.true;
  });

  it("mounts default as a text input", () => {
    const wrapper = shallowMount(BaseInput);
    const input = wrapper.find("input");
    expect(input.exists()).to.be.true;
    expect(input.attributes("type")).to.equal("text");
  });

  it("mounts default with no left or right labels", () => {
    const wrapper = shallowMount(BaseInput);
    const leftLabel = wrapper.find(".input-group-prepend");
    const rightLabel = wrapper.find(".input-group-append");
    expect(leftLabel.exists()).to.be.false;
    expect(rightLabel.exists()).to.be.false;
  });

  it("mounts with left label", () => {
    const wrapper = shallowMount(BaseInput, {
      propsData: {
        leftLabel: "$"
      }
    });
    const leftLabel = wrapper.find(".input-group-prepend");
    expect(leftLabel.exists()).to.be.true;
    expect(leftLabel.find(".input-group-text").text()).to.equal("$");
  });

  it("mounts with right label", () => {
    const wrapper = shallowMount(BaseInput, {
      propsData: {
        rightLabel: ".00"
      }
    });
    const rightLabel = wrapper.find(".input-group-append");
    expect(rightLabel.exists()).to.be.true;
    expect(rightLabel.find(".input-group-text").text()).to.equal(".00");
  });

  it("mounts small size", () => {
    const wrapper = shallowMount(BaseInput, {
      propsData: {
        rightLabel: ".00",
        size: "small"
      }
    });
    const rightLabel = wrapper.find(".input-group-sm");
    expect(rightLabel.exists()).to.be.true;
  });

  it("mounts large size", () => {
    const wrapper = shallowMount(BaseInput, {
      propsData: {
        rightLabel: ".00",
        size: "large"
      }
    });
    const rightLabel = wrapper.find(".input-group-lg");
    expect(rightLabel.exists()).to.be.true;
  });

  it("emits an event when input is changed", () => {
    const wrapper = shallowMount(BaseInput);
    const input = wrapper.find("input");
    const emitted = wrapper.emitted();
    input.setValue("1");
    expect(emitted["input-change"] && emitted["input-change"][0]).to.eql(["1"]);
  });

  it("emits an event when input is blurred", () => {
    const wrapper = shallowMount(BaseInput);
    const input = wrapper.find("input");
    const emitted = wrapper.emitted();
    input.trigger("change");
    expect(emitted["input-blur"]).to.exist;
  });

  it("updates display when value prop is changed", async () => {
    const wrapper = shallowMount(BaseInput, {
      propsData: {
        value: "value"
      }
    });
    const input = wrapper.find("input").element as HTMLInputElement;
    expect(input.value).to.equal("value");
    wrapper.setProps({ value: "other" });
    await wrapper.vm.$nextTick();
    expect(input.value).to.equal("other");
  });
});
