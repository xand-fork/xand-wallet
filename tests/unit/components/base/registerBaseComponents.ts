import { createLocalVue } from "@vue/test-utils";
import Vue from "vue";

import BaseModal from "../../../../src/components/base/BaseModal.vue";
import BaseSelect from "../../../../src/components/base/BaseSelect.vue";
import BaseInput from "../../../../src/components/base/BaseInput.vue";

export default (localVue?: typeof Vue): typeof Vue => {
  let vue = localVue;
  if (!vue) {
    vue = createLocalVue();
  }
  vue.component("BaseModal", BaseModal);
  vue.component("BaseSelect", BaseSelect);
  vue.component("BaseInput", BaseInput);
  return vue;
};
