import { mount, shallowMount } from "@vue/test-utils";
import { expect } from "chai";
import * as TransparentLogo from "../../../../src/assets/images/transparent_logo.svg";
import BaseSelect from "../../../../src/components/base/BaseSelect.vue";
import BaseSelectOption from "../../../../src/models/BaseSelectOption";

describe("BaseSelect Component", () => {
  it("mounts", () => {
    const wrapper = shallowMount(BaseSelect);
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(BaseSelect)).to.be.true;
  });

  it("renders all expected options", () => {
    const options: BaseSelectOption[] = [
      { key: 1, value: "option one" },
      { key: 2, value: "option two" }
    ];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options
      }
    });
    expect(wrapper.find(`[value='option one']`).exists()).to.be.true;
    expect(wrapper.find(`[value='option two']`).exists()).to.be.true;
  });

  it("renders disabled if set to disabled", () => {
    const options: BaseSelectOption[] = [
      { key: 1, value: "option one" },
      { key: 2, value: "option two" }
    ];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options,
        disabled: true
      }
    });
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.true;
  });

  it("renders disabled if there are no options", () => {
    const options: BaseSelectOption[] = [];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options
      }
    });
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.true;
  });

  it("renders disabled if there are no options due to initial selection being the only choice", () => {
    const options: BaseSelectOption[] = [{ key: 1, value: "option one" }];
    const wrapper = mount(BaseSelect, {
      propsData: {
        selected: 1,
        options
      }
    });
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.true;
  });

  it("renders disabled if the disabled state has changed", async () => {
    const options: BaseSelectOption[] = [{ key: 1, value: "option one" }];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options
      }
    });
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.false;
    wrapper.setProps({ disabled: true });
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.true;
  });

  it("renders disabled if the selection changes and causes no possible remaining options", async () => {
    const options: BaseSelectOption[] = [{ key: 1, value: "option one" }];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options
      }
    });
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.false;
    wrapper.setProps({ selected: 1 });
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.true;
  });

  it("renders disabled if the options are changed to 0 possible options", async () => {
    const options: BaseSelectOption[] = [{ key: 1, value: "option one" }];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options
      }
    });
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.false;
    wrapper.setProps({ options: [] });
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`[disabled='disabled']`).exists()).to.be.true;
  });

  it("renders images in options", () => {
    const options: BaseSelectOption[] = [
      { key: 1, value: "option one", imageUri: TransparentLogo },
      { key: 2, value: "option two", imageUri: TransparentLogo }
    ];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options
      }
    });
    expect(wrapper.findAll(`img`).length).to.equal(2);
  });

  it("sets a placeholder correctly with no initial selected set", () => {
    const options: BaseSelectOption[] = [
      { key: 1, value: "option one", imageUri: TransparentLogo },
      { key: 2, value: "option two", imageUri: TransparentLogo }
    ];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options,
        placeholder: "Please select an option."
      }
    });
    expect(wrapper.find(`.selection`).text()).to.equal(
      "Please select an option."
    );
  });

  it("correctly sets an initial selection", () => {
    const options: BaseSelectOption[] = [
      { key: 1, value: "option one", imageUri: TransparentLogo },
      { key: 2, value: "option two", imageUri: TransparentLogo }
    ];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options,
        placeholder: "Please select an option.",
        selected: 1
      }
    });
    expect(wrapper.find(`.selection`).text()).to.equal("option one");
  });

  it("changes selection when the selected prop changes", async () => {
    const options: BaseSelectOption[] = [
      { key: 1, value: "option one", imageUri: TransparentLogo },
      { key: 2, value: "option two", imageUri: TransparentLogo }
    ];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options,
        placeholder: "Please select an option.",
        selected: 1
      }
    });
    expect(wrapper.find(`.selection`).text()).to.equal("option one");
    wrapper.setProps({ selected: 2 });
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`.selection`).text()).to.equal("option two");
  });

  it("changes selection when an option is clicked and emits selection event", async () => {
    const options: BaseSelectOption[] = [
      { key: 1, value: "option one", imageUri: TransparentLogo },
      { key: 2, value: "option two", imageUri: TransparentLogo }
    ];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options,
        placeholder: "Please select an option.",
        selected: 1
      }
    });
    const emitted = wrapper.emitted();
    expect(wrapper.find(`.selection`).text()).to.equal("option one");
    wrapper.find(`#dropdownMenuLink`).trigger("click");
    await wrapper.vm.$nextTick();
    wrapper.find(`[value='option two']`).trigger("click");
    await wrapper.vm.$nextTick();
    expect(emitted.selection && emitted.selection[0]).to.eql([2]);
    expect(wrapper.find(`.selection`).text()).to.equal("option two");
  });

  it("handles an invalid default selection", () => {
    const options: BaseSelectOption[] = [
      { key: 3, value: "option three", imageUri: TransparentLogo },
      { key: 2, value: "option two", imageUri: TransparentLogo }
    ];
    const wrapper = mount(BaseSelect, {
      propsData: {
        options,
        placeholder: "Please select an option.",
        selected: 1
      }
    });
    expect(wrapper.find(`.selection`).text()).to.equal(
      "Please select an option."
    );
  });
});
