import MemberApiUrlInput from "@/components/bricks/MemberAPIUrlInput.vue";
import { mount, Wrapper } from "@vue/test-utils";
// import { expect } from "chai";
import registerBaseComponents from "../base/registerBaseComponents";

const localVue = registerBaseComponents();

describe("Member API Url Input", () => {
  let wrapper: Wrapper<any>;

  beforeEach(() => {
    wrapper = mount(MemberApiUrlInput, {
      computed: {
        settingsModule: () => ({
          currentMemberApiUrl: "localhost:3000"
        })
      },
      localVue
    });
  });

  // it("mounts", () => {
  //   expect(wrapper.exists()).to.be.true;
  //   expect(wrapper.is(MemberApiUrlInput)).to.be.true;
  // });

  // if there is no value in local storage it uses the env var
  it("updates member api url when input is changed", async () => {
    const inputField: HTMLInputElement = wrapper.find("input")
      .element as HTMLInputElement;
    const value = inputField.value;
    console.log("value" + value);
    // const input = wrapper.find("input");
    // console.log("starting text: " + input.text());
    // // expect(input.text()).to.equal("localhost:3000");
    // // const emitted = wrapper.emitted();
    // input.setValue("https://member-api.google.com");
    // console.log("new text: " + input.text());
    // expect(input.vm.)
    // expect(emitted["address-change"][0]).to.eql(["1"]);
  });
});
