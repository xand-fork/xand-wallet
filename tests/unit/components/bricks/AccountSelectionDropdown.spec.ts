import { mount, Wrapper } from "@vue/test-utils";
import { expect } from "chai";

import AccountSelectionDropdown from "../../../../src/components/bricks/AccountSelectionDropdown.vue";
import {
  MultipleBankSingleAccountGetters,
  MultipleBankMultipleAccountGetters
} from "../../../mocks/store/bank/mockBankGetters";
import { truncateAcctName } from "@/lib/displayBankAccount";
import { AccountNames } from "../../../mocks/data/mockBankAccounts";
import registerBaseComponents from "../base/registerBaseComponents";

const localVue = registerBaseComponents();

describe("Account Selection Dropdown Component", () => {
  let wrapper: Wrapper<any>;

  beforeEach(() => {
    wrapper = mount(AccountSelectionDropdown, {
      computed: {
        bankModule: () => ({ ...MultipleBankMultipleAccountGetters }),
        xandModule: () => ({}),
        contactsModule: () => ({}),
        userModule: () => ({})
      },
      propsData: {
        "bank-id": 2
      },
      localVue
    });
  });

  it("mounts", () => {
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(AccountSelectionDropdown)).to.be.true;
  });

  it("creates 2 options, omitting the current selection", () => {
    expect(wrapper.findAll(".dropdown-item").length).to.equal(2);
    expect(
      wrapper
        .find(
          `[value="` +
            truncateAcctName(AccountNames.coolIncChamberedTrust) +
            `: 2222 Bal: $10,000,000.00"]`
        )
        .exists()
    ).to.be.true;
    expect(
      wrapper
        .find(
          `[value="` +
            truncateAcctName(AccountNames.trustChamberedTrust) +
            `: 0000 Bal: $10,000,000.00"]`
        )
        .exists()
    ).to.be.true;
  });

  it("automatically selects the appropriate account", () => {
    expect(wrapper.find(".selection").text()).to.equal(
      truncateAcctName(AccountNames.awesomeCoChamberedTrust) +
        ": 1111 Bal: $10,000,000.00"
    );
  });

  it("changes selection and emits account-selection event on option click", async () => {
    const emitted = wrapper.emitted();
    expect(wrapper.find(`.selection`).text()).to.equal(
      truncateAcctName(AccountNames.awesomeCoChamberedTrust) +
        ": 1111 Bal: $10,000,000.00"
    );
    wrapper.find(`#dropdownMenuLink`).trigger("click");
    wrapper
      .find(
        `[value="` +
          truncateAcctName(AccountNames.trustChamberedTrust) +
          `: 0000 Bal: $10,000,000.00"]`
      )
      .trigger("click");
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`.selection`).text()).to.equal(
      truncateAcctName(AccountNames.trustChamberedTrust) +
        ": 0000 Bal: $10,000,000.00"
    );
    // Account selection event for mounting with default, and then for a new selection.
    expect(emitted["account-selection"]).to.eql([[5], [1]]);
  });

  it("handles a bank ID change", async () => {
    expect(wrapper.find(`.selection`).text()).to.equal(
      truncateAcctName(AccountNames.awesomeCoChamberedTrust) +
        ": 1111 Bal: $10,000,000.00"
    );
    wrapper.setProps({ bankId: 1 });
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`.selection`).text()).to.equal("Choose Account");
  });

  it("handles a bank ID change and then changes back to the correct account", async () => {
    expect(wrapper.find(`.selection`).text()).to.equal(
      truncateAcctName(AccountNames.awesomeCoChamberedTrust) +
        ": 1111 Bal: $10,000,000.00"
    );
    wrapper.setProps({ bankId: 1 });
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`.selection`).text()).to.equal("Choose Account");
    wrapper.setProps({ bankId: 2 });
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`.selection`).text()).to.equal(
      truncateAcctName(AccountNames.awesomeCoChamberedTrust) +
        ": 1111 Bal: $10,000,000.00"
    );
  });

  it("handles an invalid bank ID", async () => {
    wrapper.setProps({ bankId: 99999 });
    await wrapper.vm.$nextTick();
    expect(wrapper.findAll(".dropdown-item").length).to.equal(0);
    expect(wrapper.find(`.selection`).text()).to.equal("Choose Account");
  });

  it("selects the first option in a list if it is the only option", async () => {
    wrapper = mount(AccountSelectionDropdown, {
      computed: {
        bankModule: () => ({ ...MultipleBankSingleAccountGetters }),
        xandModule: () => ({}),
        contactsModule: () => ({}),
        userModule: () => ({})
      },
      propsData: {
        "bank-id": 2
      },
      localVue
    });
    expect(wrapper.find(`.selection`).text()).to.equal(
      truncateAcctName(AccountNames.trustChamberedTrust) +
        ": 0000 Bal: $10,000,000.00"
    );
    wrapper.setProps({ bankId: 1 });
    await wrapper.vm.$nextTick();
    // Autoselects x4444 when the bank switches because it's the only option.
    expect(wrapper.find(`.selection`).text()).to.equal(
      truncateAcctName(AccountNames.coolIncOctopoda) +
        ": 4444 Bal: $10,000,000.00"
    );
    wrapper.setProps({ bankId: 2 });
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`.selection`).text()).to.equal(
      truncateAcctName(AccountNames.trustChamberedTrust) +
        ": 0000 Bal: $10,000,000.00"
    );
  });
});
