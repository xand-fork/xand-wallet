import { shallowMount, mount } from "@vue/test-utils";
import { expect } from "chai";

import CurrencyInput from "../../../../src/components/bricks/CurrencyInput.vue";
import registerBaseComponents from "../base/registerBaseComponents";

const localVue = registerBaseComponents();

describe("CurrencyInput Component", () => {
  it("mounts", () => {
    const wrapper = shallowMount(CurrencyInput, { localVue });
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(CurrencyInput)).to.be.true;
  });

  it("mounts default inputValue as empty", () => {
    const wrapper = shallowMount(CurrencyInput, { localVue });
    expect(wrapper.vm.$data.inputValue).to.equal("");
  });

  it("mounts with a different initial value correctly", () => {
    const wrapper = shallowMount(CurrencyInput, {
      propsData: { initialValue: "7.12" },
      localVue
    });
    expect(wrapper.vm.$data.currentValue.decimalString).to.equal("7.12");
  });

  it("mounts default value as 0 USD Money", () => {
    const wrapper = shallowMount(CurrencyInput, { localVue });
    expect(wrapper.vm.$data.currentValue.decimalString).to.equal("0.00");
    expect(wrapper.vm.$data.currentValue.currency).to.equal("USD");
  });

  it("updates money value when input value is changed", () => {
    const wrapper = mount(CurrencyInput, { localVue });
    const input = wrapper.find("input");
    input.setValue("12.13");
    expect(wrapper.vm.$data.currentValue.decimalString).to.equal("12.13");
  });

  it("rounds money value when input value is changed (up at 5)", () => {
    const wrapper = mount(CurrencyInput, { localVue });
    const input = wrapper.find("input");
    input.setValue("12.135555555555");
    expect(wrapper.vm.$data.currentValue.decimalString).to.equal("12.14");
  });

  it("rounds money value when input value is changed (down)", () => {
    const wrapper = mount(CurrencyInput, { localVue });
    const input = wrapper.find("input");
    input.setValue("12.134121");
    expect(wrapper.vm.$data.currentValue.decimalString).to.equal("12.13");
  });

  it("results in 0 Money value with invalid input", () => {
    const wrapper = mount(CurrencyInput, { localVue });
    const input = wrapper.find("input");
    input.setValue("12.1e34.121e");
    expect(wrapper.vm.$data.currentValue.decimalString).to.equal("0.00");
  });

  it("revalidates input display on blur", async () => {
    const wrapper = mount(CurrencyInput, { localVue });
    const input = wrapper.find("input");
    input.setValue("12.135667676");
    await wrapper.vm.$nextTick();
    wrapper.vm.$data.inputValue = "12.135667676";
    wrapper.find(".input-group-prepend").trigger("click");
    await wrapper.vm.$nextTick();
    wrapper.vm.$data.inputValue = "12.14";
  });
});
