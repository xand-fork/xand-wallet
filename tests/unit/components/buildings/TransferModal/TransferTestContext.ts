import {
  APIWrapperCreationRequest,
  Receipt,
  APIWrapperRedeem,
  APIWrapperReserveTransfer
} from "@/services/IMemberApiService";
import td from "testdouble";
import Money from "tpfs-money";
import { SweetAlertType } from "sweetalert2";
import { VueClass } from "vue-class-component/lib/declarations";
import StoreModules from "@/mixins/StoreModules";
import { SingleBankSingleAccountGetters } from "../../../../mocks/store/bank/mockBankGetters";
import mockBankActions from "../../../../mocks/store/bank/mockBankActions";
import mockUserGetters from "../../../../mocks/store/user/mockUserGetters";
import mock from "../../../../mocks/store/xand/mockXandActions";
import { AllTransactionsGetters } from "../../../../mocks/store/xand/mockXandGetters";
import mockModuleActions from "../../../../mocks/store/module/mockModuleActions";
import {
  ComputedValues,
  MockValues,
  TestContext
} from "../../../../mocks/TestContext";
import flushPromises from "flush-promises";

export default class TransferModalTestContext extends TestContext {
  private computed: ComputedValues = {
    bankModule: () => ({
      ...SingleBankSingleAccountGetters,
      ...mockBankActions
    }),
    userModule: () => ({ ...mockUserGetters }),
    xandModule: () => ({ ...mock, ...AllTransactionsGetters }),
    modalModule: () => ({ ...mockModuleActions })
  };
  private mocks: MockValues = {
    getters: {
      ...mockUserGetters,
      ...SingleBankSingleAccountGetters
    },
    actions: {
      ...mockBankActions,
      ...mock,
      ...AllTransactionsGetters,
      ...mockModuleActions
    }
  };

  public constructor(component: VueClass<StoreModules>) {
    super(component);
    this.setComputed(this.computed)
      .setMocks(this.mocks)
      .enableMockApi()
      .mount();

    // Extension class' methods must be returned separately
    this.setupLogger().setupSweetAlerts();
  }

  // Getters
  public get emitted() {
    return this.wrapper!.emitted();
  }
  public get remoteLogger() {
    return this.wrapper!.vm!.$remoteLogger;
  }

  public get sweetAlert() {
    return this.wrapper!.vm.$sweetAlert; // Shortcut to plugin
  }

  public get swal(): FakeSweetAlert {
    return this.wrapper!.vm.$swal; // Instance of SweetAlert appliance
  }

  // Setup context
  public setupLogger() {
    this.wrapper!.vm.$remoteLogger = new FakeRemoteLogger();
    return this;
  }
  public setupSweetAlerts() {
    // Setup main SweetAlert mock and initialize its assertion members and methods
    this.setupSwal();
    // Setup SweetAlert plugin, using main mock
    this.setupSweetAlert();
    return this;
  }

  public setupSwal() {
    this.wrapper!.vm.$swal = new FakeSweetAlert(); // native SweetAlert mock
    return this;
  }

  public setupSweetAlert() {
    this.wrapper!.vm.$sweetAlert = this.swalFunc; // shortcut to mock plugin for SweetAlert
    return this;
  }

  // SweetAlert plugin function mock
  private swalFunc = (_icon: SweetAlertType, msg: string, _timer: number) => {
    return this.swal.fire({ text: msg });
  };

  // Formatting helpers
  public constructUSDFromMinorUnits(amt: number): Money {
    return new Money(amt, "USD");
  }

  // UI invocation helpers
  public async submitTransferModalUI(input: object) {
    await this.setModalFieldValues(input);
    await this.submitUITransfer();
    await this.cleanPromisesBeforeAsserting();
  }
  public async setModalFieldValues(input: object) {
    this.wrapper!.setData(input);
  }
  public async submitUITransfer() {
    this.wrapper!.find("#transfer-submit").trigger("submit.prevent");
  }

  public async cleanPromisesBeforeAsserting() {
    // resolve all pending promises before running assertions per Vue test utils run order - see https://vue-test-utils.vuejs.org/guides/testing-async-components.html
    await flushPromises();
  }

  // Testdouble rehearsals
  public setupCreationRequest(
    request: APIWrapperCreationRequest,
    receipt: Receipt
  ) {
    td.when(this.api!.createXAND(request)).thenResolve(receipt);
    return this;
  }

  public setupCreationRequestFailure(
    request: APIWrapperCreationRequest,
    error: Error
  ): this {
    td.when(this.api!.createXAND(request)).thenReject(error);
    return this;
  }

  public setupReserveTransferFailure(
    transfer: APIWrapperReserveTransfer,
    error: Error
  ) {
    td.when(this.api!.transferToReserve(transfer)).thenReject(error);
    return this;
  }

  public setupPollingFailure(receipt: Receipt, error: Error) {
    td.when(this.api!.pollForTxConfirmation(receipt.transactionId)).thenReject(
      error
    );
    return this;
  }

  public setupRedeem(request: APIWrapperRedeem, receipt: Receipt): this {
    td.when(this.api!.redeem(request)).thenResolve(receipt);
    return this;
  }

  public setupRedeemFailure(
    request: APIWrapperRedeem,
    error: Error
  ): TestContext {
    td.when(this.api!.redeem(request)).thenReject(error);
    return this;
  }

  // Testdouble verifications
  public verifyCreationRequest(
    submittedRequest: APIWrapperCreationRequest
  ): boolean {
    const submittedData = {
      accountId: submittedRequest.accountId,
      amountInMinorUnit: new Money(submittedRequest.amountInMinorUnit, "USD")
    };
    const vmData = {
      accountId: this.wrapper!.vm.currentSelectedAccount,
      amountInMinorUnit: this.wrapper!.vm.transferAmount
    };
    return JSON.stringify(submittedData) === JSON.stringify(vmData);
  }

  public verifyRedeem(submittedRequest: APIWrapperRedeem): boolean {
    const submittedData = {
      accountId: submittedRequest.accountId,
      amountInMinorUnit: new Money(submittedRequest.amountInMinorUnit, "USD")
    };
    const vmData = {
      accountId: this.wrapper!.vm.bankModule.currentAccountId,
      amountInMinorUnit: this.wrapper!.vm.transferAmount
    };
    return JSON.stringify(submittedData) === JSON.stringify(vmData);
  }

  // Assertion methods
  public sweetAlertsShouldContain(expectedValue: string): boolean {
    return this.swal.includes(expectedValue);
  }

  public errorLogsShouldContain(expectedValue: string): boolean {
    return this.remoteLogger.includes(expectedValue);
  }

  public errorMessageShouldBe(expectedValue: string): boolean {
    return this.wrapper!.vm.errorMsg === expectedValue;
  }
}
// tslint:disable-next-line:max-classes-per-file
export class FakeRemoteLogger {
  private logs: string[] = [];

  public includes(expectedValue: string): boolean {
    return this.logs.some(log => log.includes(expectedValue));
  }

  public error(message: string) {
    this.logs.push(message);
  }
}

// tslint:disable-next-line:max-classes-per-file
class FakeSweetAlert {
  private alerts: string[] = [];

  public includes(expectedValue: string): boolean {
    return this.alerts.some(alert => alert.includes(expectedValue));
  }

  // Mocks raw SweetAlert function
  public async fire(options: { text: string }): Promise<any> {
    this.alerts.push(options.text);
    return true;
  }
}
