import { Wrapper, mount, createLocalVue } from "@vue/test-utils";
import { expect } from "chai";

import Navbar from "@/components/sets/Navbar.vue";
import {
  MultipleBankMultipleAccountGetters,
  MultipleBankMultipleAccountDefaultNotSetGetters
} from "../../../mocks/store/bank/mockBankGetters";
import mockUserGetters from "../../../mocks/store/user/mockUserGetters";
import minorUnitCurrencyFilter from "@/plugins/minor-unit-currency-filter";
import displayMessages from "@/constants/displayMessages";
import { truncateAcctName } from "@/lib/displayBankAccount";

describe("Navbar Bank Display", () => {
  const localVue = createLocalVue();
  localVue.use(minorUnitCurrencyFilter);

  function getBankGetters(hasDefaultBankSet: boolean): any {
    return hasDefaultBankSet
      ? MultipleBankMultipleAccountGetters
      : MultipleBankMultipleAccountDefaultNotSetGetters;
  }

  function createWrapper(hasDefaultBankSet: boolean): Wrapper<any> {
    const mockBankGetters = getBankGetters(hasDefaultBankSet);
    // There's a nested component in NavBar that needs a mocked $store when not shallowMounted
    return mount(Navbar, {
      computed: {
        bankModule: () => ({ ...mockBankGetters }),
        userModule: () => ({ ...mockUserGetters }),
        xandModule: () => ({}),
        contactsModule: () => ({})
      },
      mocks: {
        $store: {
          getters: {
            ...mockBankGetters,
            ...mockUserGetters
          }
        }
      },
      localVue
    });
  }

  it("mounts", () => {
    const wrapper = createWrapper(true);
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(Navbar)).to.be.true;
  });

  it("creates no tooltip if bank account set", () => {
    const wrapper = createWrapper(true);
    const icon = wrapper.find("i#noBankToolTip");
    expect(icon.exists()).to.be.false;
  });

  it("creates tooltip with correct message if bank account not set", () => {
    const wrapper = createWrapper(false);
    const icon = wrapper.find("i#test-noBankToolTip");
    expect(icon.exists()).to.be.true;

    const value = icon?.element.attributes.getNamedItem("icon-tooltip-popup")!
      .value;
    expect(value).to.equal(displayMessages.DEFAULT_BANK_NOT_SET);
  });

  it("has bank thumbnail if bank account set", () => {
    const wrapper = createWrapper(true);
    const image = wrapper.find("img#test-bankImageSet");
    expect(image.exists()).to.be.true;
  });

  it("has no bank thumbnail if bank account not set", () => {
    const wrapper = createWrapper(false);
    const image = wrapper.find("img#test-bankImageSet");
    expect(image.exists()).to.be.false;
  });

  it("has bank name if default bank account set", () => {
    const wrapper = createWrapper(true);
    const bankName = wrapper.find("span#test-bankInfoSet").text();
    expect(bankName).contains(
      truncateAcctName("AwesomeCo's Chambered Trust checking account")
    );
  });

  it("has bank balance if default bank account set", () => {
    const wrapper = createWrapper(true);
    const bankName = wrapper.find("span#test-bankInfoSet").text();
    expect(bankName).contains("$10,000,000.00");
  });
});
