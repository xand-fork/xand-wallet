import { Wrapper, shallowMount } from "@vue/test-utils";
import { VueConstructor } from "vue/types/vue";
import { VueClass } from "vue-class-component/lib/declarations";
import td from "testdouble";
import { container } from "inversify-props";

import StoreModules from "@/mixins/StoreModules";
import { IMemberApiService } from "@/services/IMemberApiService";

import registerBaseComponents from "../unit/components/base/registerBaseComponents";

interface ComputedValues {
  [key: string]: () => Record<string, any>;
}
interface MockValues {
  [key: string]: object;
}

class TestContext {
  public readonly component: VueClass<StoreModules>;
  // { [key: string]: any } is a Typescript workaround to enable asserting on a wrapper's contents
  private _wrapper?: Wrapper<StoreModules & { [key: string]: any }>;

  private _localVue: VueConstructor<Vue>;
  private _api?: IMemberApiService;
  private _computed?: ComputedValues;
  private _mocks?: MockValues;

  constructor(component: VueClass<StoreModules>) {
    this.component = component;
    this._localVue = registerBaseComponents();
  }

  public setComputed(computed?: ComputedValues): TestContext {
    this._computed = computed || undefined;
    return this;
  }

  public setMocks(mocks?: MockValues): TestContext {
    this._mocks = mocks || undefined;
    return this;
  }

  public get wrapper() {
    return this._wrapper;
  }

  public get api() {
    return this._api;
  }

  public enableMockApi(): TestContext {
    this._api = td.object<IMemberApiService>();
    container.unbindAll();
    container
      .bind<IMemberApiService>("IMemberApiService")
      .toConstantValue(this._api);
    return this;
  }

  public mount(): TestContext {
    this._wrapper = shallowMount(this.component, {
      mocks: this._mocks,
      computed: this._computed,
      localVue: this._localVue
    });
    return this;
  }
}

export { TestContext, ComputedValues, MockValues };
