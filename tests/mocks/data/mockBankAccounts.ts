import Money from "tpfs-money";

import BankAccountState from "@/models/BankAccountState";
import { Bank } from "xand-member-api-client";

export const AccountNames = {
  coolIncChamberedTrust: "CoolInc's Chambered Trust savings account",
  coolIncOctopoda: "CoolInc's OB savings account",
  trustOctopoda: "Rusty Trustee OB account",
  awesomeCoChamberedTrust: "AwesomeCo's Chambered Trust checking account",
  trustChamberedTrust: "Rusty Trustee Chambered Trust account",
  awesomeCoOctopoda: "AwesomeCo's OB checking account`"
};

const BankAccountPropertiesForBank: {
  [id: number]: { bankId: number; routingNumber: string; bankName: string };
} = {
  1: { bankId: 1, routingNumber: "800000001", bankName: "Octopoda Bank" },
  2: { bankId: 2, routingNumber: "800000002", bankName: "Chambered Trust" }
};

export const AllBanks: Bank[] = [
  {
    id: 1,
    routingNumber: BankAccountPropertiesForBank[1].routingNumber,
    name: BankAccountPropertiesForBank[1].bankName,
    adapter: {},
    trustAccount: ""
  },
  {
    id: 2,
    routingNumber: BankAccountPropertiesForBank[2].routingNumber,
    name: BankAccountPropertiesForBank[2].bankName,
    adapter: {},
    trustAccount: ""
  }
];

export const AllBanksById = new Map(AllBanks.map(b => [b.id, b]));

export const AccountOne: BankAccountState = {
  id: 1,
  shortName: AccountNames.trustChamberedTrust,
  ...BankAccountPropertiesForBank[2],
  maskedAccountNumber: "0000",
  message: "",
  balance: new Money(1000000000, "USD")
};

export const AccountTwo: BankAccountState = {
  id: 2,
  shortName: AccountNames.coolIncOctopoda,
  ...BankAccountPropertiesForBank[1],
  maskedAccountNumber: "4444",
  message: "",
  balance: new Money(1000000000, "USD")
};

export const AccountThree: BankAccountState = {
  id: 3,
  shortName: AccountNames.awesomeCoOctopoda,
  ...BankAccountPropertiesForBank[1],
  maskedAccountNumber: "3333",
  message: "",
  balance: new Money(1000000000, "USD")
};

export const AccountFour: BankAccountState = {
  id: 4,
  shortName: AccountNames.coolIncChamberedTrust,
  ...BankAccountPropertiesForBank[2],
  maskedAccountNumber: "2222",
  message: "",
  balance: new Money(1000000000, "USD")
};

export const AccountFive: BankAccountState = {
  id: 5,
  shortName: AccountNames.awesomeCoChamberedTrust,
  ...BankAccountPropertiesForBank[2],
  maskedAccountNumber: "1111",
  message: "",
  balance: new Money(1000000000, "USD")
};

export const AccountSix: BankAccountState = {
  id: 6,
  shortName: AccountNames.trustOctopoda,
  ...BankAccountPropertiesForBank[1],
  maskedAccountNumber: "5555",
  message: "",
  balance: new Money(1000000000, "USD")
};

function constructMapping(accounts: BankAccountState[]) {
  const map = new Map<number, BankAccountState>();
  accounts.forEach((acc: BankAccountState) => map.set(acc.id, acc));
  return map;
}

export const SingleBankSingleAccountCollection = constructMapping([AccountOne]);
export const SingleBankMultipleAccountCollection = constructMapping([
  AccountOne,
  AccountFour,
  AccountFive
]);
export const MultipleBankSingleAccountCollection = constructMapping([
  AccountOne,
  AccountTwo
]);
export const MultipleBankMultipleAccountCollection = constructMapping([
  AccountOne,
  AccountTwo,
  AccountThree,
  AccountFour,
  AccountFive,
  AccountSix
]);
