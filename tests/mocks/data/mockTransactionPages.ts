import TransactionPageView from "../../../src/models/TransactionPageView";
import {
  PendingCreateRequest,
  PendingRedeemRequest,
  ConfirmedCreateRequest,
  ConfirmedRedeemRequest,
  PaymentReceived,
  PaymentSent,
  CancelledRedeemRequest,
  CancelledCreateRequest,
  UnsupportedTransaction
} from "./mockTransactions";

export const NoTransactionsPageView: TransactionPageView = {
  enrichedTxList: [],
  currentPage: 0,
  length: 0
};

export const AllTransactionsPageView: TransactionPageView = {
  enrichedTxList: [
    PendingCreateRequest,
    PendingRedeemRequest,
    CancelledRedeemRequest,
    ConfirmedCreateRequest,
    ConfirmedRedeemRequest,
    CancelledCreateRequest,
    PaymentReceived,
    PaymentSent,
    UnsupportedTransaction
  ],
  currentPage: 0,
  length: 9
};
