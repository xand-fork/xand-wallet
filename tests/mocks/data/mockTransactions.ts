import { TransactionState, OperationType } from "xand-member-api-client";
import Money from "tpfs-money";
import moment from "moment";

import displayMessages from "../../../src/constants/displayMessages";
import EnrichedTransaction from "../../../src/models/EnrichedTransaction";

import { UserFour, UserFive } from "./mockUserDetails";
import { AccountOne, AccountThree } from "./mockBankAccounts";
import { Receipt } from "xand-member-api-client";
import {
  APIWrapperCreationRequest,
  APIWrapperRedeem,
  APIWrapperReserveTransfer
} from "@/services/IMemberApiService";

// API stub transactions
export const CreateRequest: APIWrapperCreationRequest = {
  amountInMinorUnit: 1402,
  accountId: AccountOne.id
};

export const CreateReceipt: Receipt = {
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d5",
  nonce: "0xd13831440b111a35260996e72bc447fa"
};

export const RedeemReceipt: Receipt = {
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d5",
  nonce: "0xd13831440b111a35260996e72bc447fa"
};

export const Redemption: APIWrapperRedeem = {
  amountInMinorUnit: 12,
  accountId: AccountOne.id
};

export const ReserveBankTransfer: APIWrapperReserveTransfer = {
  accountId: AccountOne.id,
  amountInMinorUnit: 1402,
  nonce: "0xd13831440b111a35260996e72bc447fa"
};

// Enriched wallet transactions
export const PendingCreateRequest: EnrichedTransaction = {
  txnId: "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d4",
  toOrFromAddress: "Bank: x4444 to Wallet",
  toOrFromContact: "",
  value: 11,
  valueAsCurrency: Money.displayCurrency("en-US", "USD", 11),
  txnTypeLabel: displayMessages.txHistory.transferPending,
  isAmountDeducted: false,
  hasReceipt: false,
  latestTotalTxns: 0,
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d4",
  operation: OperationType.CreationRequest,
  signerAddress: UserFour.address,
  amountInMinorUnit: 11,
  destinationAddress: UserFour.address,
  nonce: "0xd13831440b111a35260996e72bc447fa",
  bankAccount: AccountThree,
  status: { state: TransactionState.Pending },
  datetime: "2020-05-04T20:37:24Z",
  formattedDatetime: moment("2020-05-04T20:37:24Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  )
};

export const ConfirmedCreateRequest: EnrichedTransaction = {
  txnId: "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d5",
  toOrFromAddress: "Bank: x4444 to Wallet",
  toOrFromContact: "",
  value: 11,
  valueAsCurrency: Money.displayCurrency("en-US", "USD", 11),
  txnTypeLabel: displayMessages.txHistory.transferToWallet,
  isAmountDeducted: false,
  hasReceipt: false,
  latestTotalTxns: 0,
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d5",
  operation: OperationType.CreationRequest,
  signerAddress: UserFour.address,
  amountInMinorUnit: 11,
  destinationAddress: UserFour.address,
  nonce: "0xd13831440b111a35260996e72bc447fa",
  bankAccount: AccountThree,
  status: { state: TransactionState.Confirmed },
  confirmationId:
    "0xab163d6c0507229ca95f930a0de2018d90ca903e77f032d6a1a6d4799486333a",
  datetime: "2020-05-04T20:37:24Z",
  formattedDatetime: moment("2020-05-04T20:37:24Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  )
};

export const CancelledCreateRequest: EnrichedTransaction = {
  txnId: "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d6",
  toOrFromAddress: "Bank: x4444 to Wallet",
  toOrFromContact: "",
  value: 11,
  valueAsCurrency: Money.displayCurrency("en-US", "USD", 11),
  txnTypeLabel: displayMessages.txHistory.transferCancelled,
  isAmountDeducted: false,
  hasReceipt: false,
  latestTotalTxns: 0,
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d6",
  operation: OperationType.CreationRequest,
  signerAddress: UserFour.address,
  amountInMinorUnit: 11,
  destinationAddress: UserFour.address,
  nonce: "0xd13831440b111a35260996e72bc447fa",
  bankAccount: AccountThree,
  status: { state: TransactionState.Cancelled },
  cancellationId:
    "0xab163d6c0507229ca95f930a0de2018d90ca903e77f032d6a1a6d4799486333a",
  datetime: "2020-05-04T20:37:24Z",
  formattedDatetime: moment("2020-05-04T20:37:24Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  )
};

export const PendingRedeemRequest: EnrichedTransaction = {
  txnId: "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d7",
  toOrFromAddress: "Wallet to Bank: x4444",
  toOrFromContact: "",
  value: 11,
  valueAsCurrency: Money.displayCurrency("en-US", "USD", 11),
  txnTypeLabel: displayMessages.txHistory.transferPending,
  isAmountDeducted: true,
  hasReceipt: false,
  latestTotalTxns: 0,
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d7",
  operation: OperationType.Redeem,
  signerAddress: UserFour.address,
  amountInMinorUnit: 11,
  destinationAddress: UserFour.address,
  nonce: "0xd13831440b111a35260996e72bc447fa",
  bankAccount: AccountThree,
  status: { state: TransactionState.Pending },
  datetime: "2020-05-04T20:37:24Z",
  formattedDatetime: moment("2020-05-04T20:37:24Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  )
};

export const ConfirmedRedeemRequest: EnrichedTransaction = {
  txnId: "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d8",
  toOrFromAddress: "Wallet to Bank: x4444",
  toOrFromContact: "",
  value: 11,
  valueAsCurrency: Money.displayCurrency("en-US", "USD", 11),
  txnTypeLabel: displayMessages.txHistory.transferToBank,
  isAmountDeducted: true,
  hasReceipt: false,
  latestTotalTxns: 0,
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d8",
  operation: OperationType.Redeem,
  signerAddress: UserFour.address,
  amountInMinorUnit: 11,
  destinationAddress: UserFour.address,
  nonce: "0xd13831440b111a35260996e72bc447fa",
  bankAccount: AccountThree,
  status: { state: TransactionState.Confirmed },
  confirmationId:
    "0xab163d6c0507229ca95f930a0de2018d90ca903e77f032d6a1a6d4799486333a",
  datetime: "2020-05-04T20:37:24Z",
  formattedDatetime: moment("2020-05-04T20:37:24Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  )
};

export const CancelledRedeemRequest: EnrichedTransaction = {
  txnId: "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d9",
  toOrFromAddress: "Wallet to Bank: x4444",
  toOrFromContact: "",
  value: 11,
  valueAsCurrency: Money.displayCurrency("en-US", "USD", 11),
  txnTypeLabel: displayMessages.txHistory.transferCancelled,
  isAmountDeducted: true,
  hasReceipt: false,
  latestTotalTxns: 0,
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2d9",
  operation: OperationType.Redeem,
  signerAddress: UserFour.address,
  amountInMinorUnit: 11,
  destinationAddress: UserFour.address,
  nonce: "0xd13831440b111a35260996e72bc447fa",
  bankAccount: AccountThree,
  status: { state: TransactionState.Cancelled },
  cancellationId:
    "0xab163d6c0507229ca95f930a0de2018d90ca903e77f032d6a1a6d4799486333a",
  datetime: "2020-05-04T20:37:24Z",
  formattedDatetime: moment("2020-05-04T20:37:24Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  )
};

export const PaymentSent: EnrichedTransaction = {
  amountInMinorUnit: 190,
  datetime: "2020-05-04T17:39:12Z",
  formattedDatetime: moment("2020-05-04T17:39:12Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  ),
  destinationAddress: UserFive.address,
  hasReceipt: true,
  isAmountDeducted: true,
  latestTotalTxns: 0,
  operation: OperationType.Payment,
  signerAddress: UserFour.address,
  status: { state: TransactionState.Confirmed },
  toOrFromAddress: UserFive.address!,
  toOrFromContact: "Friend - FriendCo",
  transactionId:
    "0x767831d900f63e3d51de48f271eec1c0eebb60b4730dc9383daf200c90b7563b",
  txnId: "0x767831d900f63e3d51de48f271eec1c0eebb60b4730dc9383daf200c90b7563b",
  txnTypeLabel: displayMessages.txHistory.paymentSent,
  value: 190,
  valueAsCurrency: Money.displayCurrency("en-US", "USD", 190)
};

export const PaymentReceived: EnrichedTransaction = {
  amountInMinorUnit: 190,
  datetime: "2020-05-04T17:39:12Z",
  formattedDatetime: moment("2020-05-04T17:39:12Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  ),
  destinationAddress: UserFive.address,
  hasReceipt: true,
  isAmountDeducted: false,
  latestTotalTxns: 0,
  operation: OperationType.Payment,
  signerAddress: UserFour.address,
  status: { state: TransactionState.Confirmed },
  toOrFromAddress: UserFive.address!,
  toOrFromContact: "Friend - FriendCo",
  transactionId:
    "0x767831d900f63e3d51de48f271eec1c0eebb60b4730dc9383daf200c90b7563c",
  txnId: "0x767831d900f63e3d51de48f271eec1c0eebb60b4730dc9383daf200c90b7563c",
  txnTypeLabel: displayMessages.txHistory.paymentReceived,
  value: 190,
  valueAsCurrency: Money.displayCurrency("en-US", "USD", 190)
};

export const UnsupportedTransaction: EnrichedTransaction = {
  txnId: "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2da",
  toOrFromAddress: "N/A",
  toOrFromContact: "",
  value: "N/A",
  valueAsCurrency: "N/A",
  txnTypeLabel: "N/A",
  isAmountDeducted: true,
  hasReceipt: false,
  latestTotalTxns: 0,
  transactionId:
    "0x1e25eb426940df2d70d0d99befa2abfc70cccdd50c73876e739b57a9c2a5e2da",
  operation: OperationType.RedeemFulfillment,
  signerAddress: UserFour.address,
  amountInMinorUnit: 11,
  destinationAddress: UserFour.address,
  nonce: "",
  bankAccount: AccountThree,
  status: { state: TransactionState.Confirmed },
  confirmationId:
    "0xab163d6c0507229ca95f930a0de2018d90ca903e77f032d6a1a6d4799486333a",
  datetime: "2020-05-04T20:37:24Z",
  formattedDatetime: moment("2020-05-04T20:37:24Z").format(
    displayMessages.LOCALIZED_DATETIME_FORMAT
  )
};
