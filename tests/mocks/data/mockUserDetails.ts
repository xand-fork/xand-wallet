import UserDetails from "../../../src/models/UserDetails";

export const UserOne: UserDetails = {
  address: "OCYwaIEztro8hLZQ8Si4SwGTsn7AiRpkr7Ve3kNI9m5W5yE4",
  registered: true,
  userBalanceInMinorUnit: 100,
  lastRefreshedDateTime: ""
};

export const UserTwo: UserDetails = {
  address: "e9IpTD1eNZPJbHFEwKpvf77sUQXy0cOjOGqar3zflTLo6p2T",
  registered: true,
  userBalanceInMinorUnit: 0,
  lastRefreshedDateTime: ""
};

export const UserThree: UserDetails = {
  address: "dLKCH8o7FvUtfUdPKLspOvzEHlBsU6y4gVuKToORXCgu30us",
  registered: true,
  userBalanceInMinorUnit: -100,
  lastRefreshedDateTime: ""
};

export const UserFour: UserDetails = {
  address: "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ",
  registered: true,
  userBalanceInMinorUnit: 12000,
  lastRefreshedDateTime: ""
};

export const UserFive: UserDetails = {
  address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ",
  registered: true,
  userBalanceInMinorUnit: 99999,
  lastRefreshedDateTime: ""
};

export default [UserOne, UserTwo, UserThree, UserFour, UserFive];
