import TransactionPageView from "../../../../src/models/TransactionPageView";
import { UserFour } from "../../data/mockUserDetails";
import {
  NoTransactionsPageView,
  AllTransactionsPageView
} from "../../data/mockTransactionPages";

interface XandGetters {
  recipient: string | null;
  transactionView: TransactionPageView;
  address: string;
}

export const NoTransactionsGetters: XandGetters = {
  recipient: null,
  address: UserFour.address!,
  transactionView: NoTransactionsPageView
};

export const AllTransactionsGetters: XandGetters = {
  recipient: null,
  address: UserFour.address!,
  transactionView: AllTransactionsPageView
};
