export default {
  clearAllAccounts: () => {
    return;
  },
  refreshAllAccounts: () => {
    return;
  },
  refreshAllBanks: () => {
    return;
  },
  loadBankAccountId: () => {
    return;
  },
  updateCurrentAccount: (s: string) => s,
  refreshAllBalances: () => {
    return;
  },
  refreshBalance: (s: string) => s
};
