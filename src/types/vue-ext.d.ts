import dialog from "electron";
import TransactionToaster from "@/lib/TransactionToaster";

declare module "vue/types/vue" {
  interface Vue {
    $toast: Toastr;
    $txToast: TransactionToaster;
    $swal: any;
    $sweetAlert: any;
    $remoteLogger: any;
    $remoteDialog: dialog.Dialog;
    $getLoggerError: any;
    $getSwalContent: any;
  }
}
