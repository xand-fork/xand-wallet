import { Inject } from "inversify-props";
import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";
import Money from "tpfs-money";

import displayMessages from "@/constants/displayMessages";
import __env from "@/env";
import { IUserSettingsStorage } from "@/persistence/interfaces";
import { IMemberApiService } from "@/services/IMemberApiService";
import BankAccountState from "@/models/BankAccountState";
import { Bank, BankAccount } from "xand-member-api-client";
import { BASE_SELECT_NONEXISTENT_ID } from "@/constants/baseSelect";

async function getAccounts(): Promise<BankAccount[]> {
  const injections = new BankInjection();
  let accs = await injections.api.getAccounts();
  if (__env.BANK_ACCOUNT_ID_FILTER && __env.BANK_ACCOUNT_ID_FILTER.length > 0) {
    accs = accs.filter((acc: BankAccount) =>
      __env.BANK_ACCOUNT_ID_FILTER.includes(acc.id)
    );
  }
  return accs;
}

async function getBanks(): Promise<Bank[]> {
  const injections = new BankInjection();
  return await injections.api.getBanks();
}

function accountsToStates(accs: BankAccount[]): BankAccountState[] {
  return accs.map((acc: BankAccount) => {
    return { ...acc, message: "", balance: new Money(0, "USD") };
  });
}

class BankInjection {
  @Inject("IUserSettingsStorage")
  public userSettingsStorage!: IUserSettingsStorage;

  @Inject("IMemberApiService")
  public api!: IMemberApiService;
}

// tslint:disable-next-line: max-classes-per-file
@Module({ namespaced: false, name: "BankStore" })
export default class BankStore extends VuexModule {
  private accounts: Map<number, BankAccountState> = new Map<
    number,
    BankAccountState
  >();
  private banks = new Map<number, Bank>();
  private _currentAccountId: number = BASE_SELECT_NONEXISTENT_ID;

  get allAccounts() {
    return this.accounts;
  }

  get currentAccount() {
    return this.accounts.get(this._currentAccountId);
  }

  get currentAccountId() {
    return this._currentAccountId;
  }

  get allBanks() {
    return this.banks;
  }

  get accountById() {
    return (id: number) => this.accounts.get(id);
  }

  @Mutation
  public setAccounts(accs: BankAccountState[]) {
    this.accounts.clear();
    accs.forEach((acc: BankAccountState) => this.accounts.set(acc.id, acc));
    this.accounts = new Map(this.accounts);
  }

  @Mutation
  public setBanks(banks: Bank[]) {
    this.banks.clear();
    banks.forEach((bank: Bank) => this.banks.set(bank.id, bank));
    this.banks = new Map(this.banks);
  }

  @Mutation
  public setCurrentAccount({
    id,
    walletAddr
  }: {
    id: number;
    walletAddr?: string;
  }) {
    this._currentAccountId = id;
    if (walletAddr) {
      const injections = new BankInjection();
      injections.userSettingsStorage.setMRUBankAccount(walletAddr, id);
    }
  }

  @Mutation
  public updateAccountBalance({ id, balance }: { id: number; balance: Money }) {
    const account = this.accounts.get(id);
    if (!account) {
      return;
    }
    account.balance = balance;
    this.accounts.set(id, account);
    this.accounts = new Map(this.accounts);
  }

  @Mutation
  public updateAccountMessage({
    id,
    message
  }: {
    id: number;
    message: string;
  }) {
    const account = this.accounts.get(id);
    if (!account) {
      return;
    }
    account.message = message;
    this.accounts.set(id, account);
    this.accounts = new Map(this.accounts);
  }

  @Action({ rawError: true })
  public async refreshAllAccounts() {
    const accs = await getAccounts();
    this.context.commit("setAccounts", accountsToStates(accs));
    await this.context.dispatch("loadBankAccountId");
    await this.context.dispatch("refreshAllBalances");
  }

  @Action({ rawError: true })
  public async refreshAllBanks() {
    const banks = await getBanks();
    this.context.commit("setBanks", banks);
  }

  @Action({ rawError: true })
  public async clearAllAccounts() {
    this.context.commit("setAccounts", []);
    this.context.commit("setCurrentAccount", { id: "" });
  }

  @Action({ rawError: true })
  public async loadBankAccountId() {
    let accountId = BASE_SELECT_NONEXISTENT_ID;
    const userDetails = this.context.rootGetters.userDetails;
    const injections = new BankInjection();

    // MRU
    if (userDetails.address) {
      const mruAccount = injections.userSettingsStorage.getMRUBankAccount(
        userDetails.address
      );
      if (mruAccount) {
        accountId = mruAccount;
      }
    }

    const accounts: BankAccountState[] = Array.from(
      this.context.getters.allAccounts.values()
    );

    // Only one account possible
    if (accountId === BASE_SELECT_NONEXISTENT_ID && accounts.length === 1) {
      accountId = accounts[0].id || BASE_SELECT_NONEXISTENT_ID;
    }

    this.context.commit("setCurrentAccount", {
      id: accountId,
      walletAddr: userDetails.address
    });
  }

  @Action({ rawError: true })
  public async updateCurrentAccount(id: number) {
    const walletAddr = this.context.rootGetters.userDetails.address;
    this.context.commit("setCurrentAccount", { id, walletAddr });
  }

  @Action({ rawError: true })
  public async refreshAllBalances() {
    const refreshPromises: Array<Promise<void>> = [];
    this.context.getters.allAccounts.forEach((acc: BankAccountState) => {
      return refreshPromises.push(
        this.context.dispatch("refreshBalance", acc.id)
      );
    });
    await Promise.all(refreshPromises);
  }

  @Action({ rawError: true })
  public async refreshBalance(id: number) {
    if (!this.context.getters.allAccounts.get(id)) {
      return;
    }
    try {
      const injections = new BankInjection();
      const {
        availableBalanceInMinorUnit
      } = await injections.api.getAccountBalance(id);
      const balance = new Money(availableBalanceInMinorUnit || 0, "USD");
      this.context.commit("updateAccountBalance", { id, balance });
      this.context.commit("updateAccountMessage", { id, message: "" });
    } catch (err) {
      this.context.commit("updateAccountMessage", {
        id,
        message: displayMessages.ACCOUNT_BANK_BALANCE_FETCH_STUCK
      });
    }
  }
}
