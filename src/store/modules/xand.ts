import { Inject } from "inversify-props";
import { Module, Action, Mutation, VuexModule } from "vuex-module-decorators";

import TransactionPageView from "@/models/TransactionPageView";
import { IMemberApiService } from "@/services/IMemberApiService";
import TransactionToaster from "@/lib/TransactionToaster";
import enrichTransactionList from "@/lib/enrichTransactionList";

const maxHistorySize = 200;
const maxHistoryPage = 0;

class XandInjection {
  @Inject("IMemberApiService")
  public api!: IMemberApiService;
}

// tslint:disable-next-line: max-classes-per-file
@Module({ namespaced: false, name: "xand" })
export default class XandStore extends VuexModule {
  private currentTransactionHistory: TransactionPageView = {
    enrichedTxList: [],
    currentPage: 0,
    length: 0
  };

  private sendMoneyRecipient: string | null = null;

  public get recipient(): string | null {
    return this.sendMoneyRecipient;
  }

  public get transactionView(): TransactionPageView {
    return this.currentTransactionHistory;
  }

  public get address(): string {
    return this.context.rootGetters.userDetails.address as string;
  }

  @Mutation
  public setRecipient(recipient: string | null) {
    this.sendMoneyRecipient = recipient;
  }

  @Mutation
  public setTransactionPageView(view: TransactionPageView) {
    const { enrichedTxList, currentPage, length } = view;
    this.currentTransactionHistory.enrichedTxList = enrichedTxList;
    this.currentTransactionHistory.currentPage = currentPage;
    this.currentTransactionHistory.length = length;
  }

  @Action({ rawError: true })
  public getMemberApiService(): IMemberApiService {
    const injections = new XandInjection();
    return injections.api;
  }

  @Action({ rawError: true })
  public async getTransactionHistoryPage(payload: {
    pagination?: { pageNumber: number; pageSize: number };
    toaster: TransactionToaster;
  }) {
    const { toaster } = payload;
    if (
      !this.context.getters.userDetails ||
      !this.context.getters.userDetails.address
    ) {
      return;
    }

    try {
      const injections = new XandInjection();
      // Until pagination is implemented in the UI we have decided to have a max of 200 transactions in history.
      // Sending "0" for a page size will indicate getting all transactions so don't do that!
      const rawHist = await injections.api.getHistory({
        pageSize: maxHistorySize,
        pageNumber: maxHistoryPage
      });
      if (Array.isArray(rawHist.transactions)) {
        const txTotal = rawHist.total;
        const rawTxns = rawHist.transactions;
        if (rawTxns != null) {
          const txnList = rawTxns.reverse();
          const enrichedTxns = enrichTransactionList(
            txnList,
            txTotal || 0,
            this.context.rootGetters.contacts,
            this.context.rootGetters.userDetails.address
          );
          toaster.checkAndNotify(enrichedTxns);
          this.context.commit("setTransactionPageView", {
            enrichedTxList: enrichedTxns,
            currentPage: maxHistoryPage,
            length: rawHist.total
          });
        }
      }
      toaster.setReady(true);
    } catch (err) {
      throw err;
    }
  }
}
