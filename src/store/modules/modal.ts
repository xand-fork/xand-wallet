import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";

import ModalStorePayload from "@/models/ModalStorePayload";

@Module({ namespaced: false, name: "modal" })
export default class ModalStore extends VuexModule {
  public currentModalState: ModalStorePayload | undefined = undefined;

  get currentModal() {
    return this.currentModalState;
  }

  @Mutation
  public updateCurrentModal(newModal: ModalStorePayload | undefined) {
    this.currentModalState = newModal;
  }

  @Action({ rawError: true })
  public async openOrReplaceModal(newModal: ModalStorePayload | undefined) {
    this.context.commit("updateCurrentModal", newModal);
  }

  @Action({ rawError: true })
  public async closeModal() {
    this.context.commit("updateCurrentModal", undefined);
  }
}
