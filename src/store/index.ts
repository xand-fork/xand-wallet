import Vue from "vue";
import Vuex from "vuex";

import BankStore from "./modules/bank";
import UserStore from "./modules/user";
import XandStore from "./modules/xand";
import ContactStore from "./modules/contact";
import ModalStore from "./modules/modal";
import SettingsStore from "./modules/settings";

// Install Vuex plugin
Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    ContactStore,
    UserStore,
    XandStore,
    BankStore,
    ModalStore,
    SettingsStore
  }
});

export default store;
