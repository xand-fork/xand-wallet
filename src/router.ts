import Router from "vue-router";

import Home from "@/components/boxes/Home.vue";
import Receipt from "@/components/boxes/Receipt.vue";
import Send from "@/components/boxes/Send.vue";

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/receipt",
      name: "receipt",
      component: Receipt,
      props: true
    },
    {
      path: "/send",
      name: "send",
      component: Send
    }
  ]
});
