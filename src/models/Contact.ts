export default interface Contact {
  contactName?: string;
  company: string;
  address: string;
}
