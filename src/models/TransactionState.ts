import TransactionPageView from "./TransactionPageView";

export default interface TransactionState {
  currentTransactionHistory: TransactionPageView;
  sendMoneyRecipient: string;
}
