import Money from "tpfs-money";
import { BankAccount } from "xand-member-api-client";

export default interface BankAccountState extends BankAccount {
  message: string;
  balance: Money;
}
