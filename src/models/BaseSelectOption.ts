export default interface BaseSelectOption {
  key: number;
  value: string;
  imageUri?: string;
}
