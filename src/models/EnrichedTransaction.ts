import { Transaction } from "xand-member-api-client";

export default interface EnrichedTransaction extends Transaction {
  txnId: string;
  toOrFromAddress: string;
  toOrFromContact: string;
  value: EnrichedTransactionValue;
  valueAsCurrency: string;
  txnTypeLabel: string;
  isAmountDeducted: boolean | undefined;
  hasReceipt: boolean;
  latestTotalTxns: number;
  formattedDatetime: string;
}
