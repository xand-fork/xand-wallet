import Contact from "./Contact";

export default interface ContactsState {
  contacts: { [key: string]: Contact };
}
