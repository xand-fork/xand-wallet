import Swal, { SweetAlertType } from "sweetalert2";
import _Vue from "vue";

export default {
  install(Vue: typeof _Vue) {
    const swalFunc = (
      iconInp: SweetAlertType,
      textInp: string,
      timerInp: number = 0
    ) => {
      const customClass = {
        closeButton: "btn btn-lg f18 btn-swal",
        confirmButton: "btn btn-lg f18 btn-swal",
        cancelButton: "btn btn-danger btn-lg f18 btn-swal-cancel"
      };
      // Show a confirm button if the timerInp is 0.
      return Swal.fire({
        type: iconInp,
        html: textInp,
        timer: timerInp,
        showConfirmButton: timerInp === 0,
        buttonsStyling: false,
        customClass
      });
    };
    // add the instance method
    if (!Vue.prototype.hasOwnProperty("$sweetAlert")) {
      Object.defineProperty(Vue.prototype, "$sweetAlert", {
        get: function get() {
          return swalFunc;
        }
      });
    }
    // add the instance method
    if (!Vue.prototype.hasOwnProperty("$swal")) {
      Object.defineProperty(Vue.prototype, "$swal", {
        get: function get() {
          return Swal;
        }
      });
      Vue.prototype.$getSwalContent = Swal.getContent();
    }
  }
};
