import Vue from "vue";
import vueCurrencyInput from "vue-currency-input";
import Router from "vue-router";

import buildDependencyContainer from "@/app.container";
import App from "@/App.vue";
import router from "@/router";
import { default as Electron } from "@/plugins/electron";
import minorUnitCurrencyFilter from "@/plugins/minor-unit-currency-filter";
import sweetalert from "@/plugins/sweetalert";
import toastr from "@/plugins/toastr";
import store from "@/store";
import isDev from "electron-is-dev";
import registerBaseComponents from "@/components/base/registerBaseComponents";

// Popper is needed to ensure our popups works correctly
import "popper.js";
import "bootstrap";
import "@/lib/TransactionToaster";

buildDependencyContainer();
registerBaseComponents();

Vue.use(Router);
Vue.use(Electron);
Vue.use(sweetalert);
Vue.use(vueCurrencyInput);
Vue.use(minorUnitCurrencyFilter);
Vue.use(toastr);
Vue.config.productionTip = false;
Vue.config.devtools = isDev;
Vue.config.performance = isDev;

new Vue({
  router,
  store,
  render: (h: any) => h(App)
}).$mount("#app");
