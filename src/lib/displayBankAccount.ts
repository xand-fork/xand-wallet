import BigNumber from "bignumber.js";
import Money from "tpfs-money";
import { BankAccount } from "xand-member-api-client";

import BankAccountState from "@/models/BankAccountState";

const maxDisplayNameChars = 20;

export function truncateAcctName(name: string): string {
  return name.slice(0, maxDisplayNameChars);
}

export function displayBankAccountBalance(
  account: BankAccount | BankAccountState,
  balance: number | string | BigNumber | Money
) {
  if (balance instanceof Money) {
    return `${truncateAcctName(account.shortName || "")}: ${
      account.maskedAccountNumber
    } Bal: ${balance}`;
  }
  return `${truncateAcctName(account.shortName || "")}: ${
    account.maskedAccountNumber
  } Bal: ${Money.displayCurrency("en-US", "USD", balance)}`;
}

export function displayBankAccountStateBalance(account: BankAccountState) {
  return displayBankAccountBalance(account, account.balance);
}
