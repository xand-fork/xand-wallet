import {
  Transaction,
  OperationType,
  TransactionState
} from "xand-member-api-client";
import Money from "tpfs-money";
import moment from "moment";

import EnrichedTransaction from "@/models/EnrichedTransaction";
import displayMessages from "@/constants/displayMessages";
import displayContactName from "@/lib/displayContactName";

export default function enrichTransactionList(
  txList: Transaction[],
  txTotal: number,
  contacts: any,
  memberAddress: string
): EnrichedTransaction[] {
  return txList.map((t: Transaction) => {
    const txnId = t.transactionId || "";
    const formattedDatetime = moment(t.datetime).format(
      displayMessages.LOCALIZED_DATETIME_FORMAT
    );
    let value: EnrichedTransactionValue = "N/A";
    let valueAsCurrency = "N/A";
    let addressInContacts = { contactName: undefined, company: "" };
    let toOrFromAddress = "";
    let toOrFromContact = "";
    let txnTypeLabel = "";
    let isAmountDeducted: boolean | undefined;
    let hasReceipt = false;
    const bankName = t.bankAccount ? t.bankAccount.bankName : "";
    const maskedAccountNumber = t.bankAccount
      ? t.bankAccount.maskedAccountNumber
      : "";
    const bankAccountLabel = t.bankAccount
      ? `${bankName}: x${maskedAccountNumber}`
      : "Bank";
    const latestTotalTxns = txTotal;
    switch (t.operation) {
      case OperationType.CreationRequest.toString():
        isAmountDeducted = false;
        value = t.amountInMinorUnit || 0;
        txnTypeLabel =
          t.status.state === TransactionState.Confirmed
            ? displayMessages.txHistory.transferToWallet
            : getUnconfirmedLabel(t);
        toOrFromAddress = `${bankAccountLabel} to Wallet`;
        break;
      case OperationType.Redeem.toString():
        isAmountDeducted = true;
        value = t.amountInMinorUnit || 0;
        txnTypeLabel =
          t.status.state === TransactionState.Confirmed
            ? displayMessages.txHistory.transferToBank
            : getUnconfirmedLabel(t);
        toOrFromAddress = `Wallet to ${bankAccountLabel}`;
        break;
      case OperationType.Payment.toString():
        txnTypeLabel = displayMessages.txHistory.transfer;
        isAmountDeducted = t.signerAddress === memberAddress;
        value = t.amountInMinorUnit || 0;
        if (isAmountDeducted) {
          addressInContacts = contacts[t.destinationAddress || ""];
          if (addressInContacts) {
            toOrFromContact = displayContactName({
              ...addressInContacts,
              address: t.destinationAddress || ""
            });
          }

          toOrFromAddress = t.destinationAddress || "";
          hasReceipt = true;
          txnTypeLabel = displayMessages.txHistory.paymentSent;
        } else {
          addressInContacts = contacts[t.signerAddress || ""];
          if (addressInContacts) {
            toOrFromContact = displayContactName({
              ...addressInContacts,
              address: t.signerAddress || ""
            });
          }

          toOrFromAddress = t.signerAddress || "";
          hasReceipt = true;
          txnTypeLabel = displayMessages.txHistory.paymentReceived;
        }
        break;
      default:
        value = "N/A";
        txnTypeLabel = "N/A";
        toOrFromAddress = `N/A`;
        break;
    }

    valueAsCurrency =
      value === "N/A" ? "N/A" : Money.displayCurrency("en-US", "USD", value);

    return {
      ...t,
      ...{
        txnId,
        toOrFromAddress,
        toOrFromContact,
        value,
        valueAsCurrency,
        txnTypeLabel,
        isAmountDeducted,
        hasReceipt,
        latestTotalTxns,
        formattedDatetime
      }
    };
  });
}

function getUnconfirmedLabel(t: Transaction): string {
  if (t.status.state === TransactionState.Pending) {
    return displayMessages.txHistory.transferPending;
  }

  if (t.status.state === TransactionState.Cancelled) {
    return displayMessages.txHistory.transferCancelled;
  }

  return "N/A";
}
