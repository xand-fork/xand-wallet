import Writer from "../Writer";

export default interface Stream {
  openWrite(writer: (writer: Writer) => any): any;
}
