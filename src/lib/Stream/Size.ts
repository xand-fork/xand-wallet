export default class Size {
  private sizeInBytes: number;

  constructor(sizeInBytes: number) {
    this.sizeInBytes = sizeInBytes;
  }

  public bytes(): number {
    return this.sizeInBytes;
  }

  public kilobytes(): number {
    return this.sizeInBytes / 1024;
  }

  public megabytes(): number {
    return this.kilobytes() / 1024;
  }

  public gigabytes(): number {
    return this.megabytes() / 1024;
  }

  public terabytes(): number {
    return this.gigabytes() / 1024;
  }
}
