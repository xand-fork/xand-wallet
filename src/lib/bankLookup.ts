import bankLookupTable from "@/constants/bankLookupTable";
import BankInfo from "@/models/BankInfo";

const bankLookup: Map<string, BankInfo> = new Map(bankLookupTable);

export default bankLookup;
