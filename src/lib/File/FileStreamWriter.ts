import fs from "fs";
import Writer from "@/lib/Writer";
export class FileStreamWriter implements Writer {
  public inner: fs.WriteStream;
  constructor(inner: fs.WriteStream) {
    this.inner = inner;
  }
  public write<T>(input: T) {
    this.inner.write(`${input}`);
  }
  public writeLine<T>(input: T) {
    this.inner.write(`${input}\n`);
  }
}
