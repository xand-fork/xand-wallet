import Stream from "@/lib/Stream/Stream";
import fs from "fs";
import Size from "@/lib/Stream/Size";
import Writer from "@/lib/Writer";
import { FileStreamWriter } from "./FileStreamWriter";

export default class File implements Stream {
  private fileName: string;

  constructor(fileName: string) {
    this.fileName = fileName;
  }

  public exists(): boolean {
    return fs.existsSync(this.fileName);
  }

  public delete() {
    fs.unlinkSync(this.fileName);
  }

  public size(): Size {
    const stats = fs.statSync(this.fileName);
    return new Size(stats.size);
  }

  public async openWrite(writer: (writer: Writer) => any) {
    const fsWriteStream = fs.createWriteStream(this.fileName);

    const fileStreamWriter = new FileStreamWriter(fsWriteStream);
    writer(fileStreamWriter);

    await new Promise<string>((resolve, _reject) => {
      fsWriteStream.end(() => resolve(this.fileName));
    });
  }
}
