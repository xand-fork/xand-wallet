import { Currency } from "tpfs-money";

import currencyDataTable from "@/constants/currencyDataTable";
import CurrencyData from "@/models/CurrencyData";

const currencyDataLookup: Map<Currency, CurrencyData> = new Map(
  currencyDataTable
);

export default currencyDataLookup;
