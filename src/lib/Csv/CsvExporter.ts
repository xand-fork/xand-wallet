import { Transaction } from "xand-member-api-client";
import ExportableTransaction from "@/models/ExportableTransaction";
import Stream from "@/lib/Stream/Stream";

import { IMemberApiService } from "@/services/IMemberApiService";

export default class CsvExporter {
  private stream: Stream;
  private memberAddress: string;
  public service: IMemberApiService;

  constructor(
    stream: Stream,
    memberAddress: string,
    service: IMemberApiService
  ) {
    this.stream = stream;
    this.memberAddress = memberAddress;
    this.service = service;
  }

  public async export() {
    const page = 1;
    const size = 125000;
    const history = await this.service.getHistory({
      pageNumber: page,
      pageSize: size
    });

    if (!history) {
      throw new Error("No history returned from api call.");
    }

    const transactions: Transaction[] = history.transactions || [];

    await this.stream.openWrite(writer => {
      /*
        This impl keeps things at O(n) for perf.
      */
      writer.writeLine(ExportableTransaction.header());

      for (const transaction of transactions) {
        const mapped = ExportableTransaction.fromTransaction(
          transaction,
          this.memberAddress
        );
        writer.writeLine(mapped);
      }
    });
  }
}
