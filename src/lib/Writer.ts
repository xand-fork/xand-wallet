export default interface Writer {
  write<T>(input: T): any;
  writeLine<T>(input: T): any;
}
