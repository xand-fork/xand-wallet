import { container } from "inversify-props";
import "reflect-metadata";

import { IMemberApiService } from "@/services/IMemberApiService";
import MemberApiService from "@/services/MemberApiService";

import { IUserSettingsStorage } from "@/persistence/interfaces";
import UserSettingsStorage from "@/persistence/UserSettingsStorage";

export default function buildDependencyContainer(): void {
  container.addSingleton<IUserSettingsStorage>(
    UserSettingsStorage,
    "IUserSettingsStorage"
  );
  container.addSingleton<IMemberApiService>(
    MemberApiService,
    "IMemberApiService"
  );
  container.bind<Storage>("Storage").toConstantValue(localStorage);
}
