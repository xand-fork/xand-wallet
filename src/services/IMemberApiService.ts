import {
  XandBalance,
  TransactionHistory,
  Transaction,
  BankAccount,
  BankAccountBalance,
  Member,
  TransferReceipt,
  Receipt,
  Bank
} from "xand-member-api-client";

interface IMemberApiService {
  updateUrl: (url: string) => void;
  updateJwt: (token: string | undefined) => void;

  getMember: () => Promise<Member>;

  getXANDBalance: () => Promise<XandBalance>;

  transfer: ({
    toAddress,
    amountInMinorUnit
  }: APIWrapperTransfer) => Promise<Receipt>;

  createXAND: ({
    accountId,
    amountInMinorUnit
  }: APIWrapperCreationRequest) => Promise<Receipt>;

  redeem: ({
    accountId,
    amountInMinorUnit
  }: APIWrapperRedeem) => Promise<Receipt>;

  getHistory: ({
    pageSize,
    pageNumber
  }: APIWrapperHistoryRequest) => Promise<TransactionHistory>;

  getTransaction: (transactionId: string) => Promise<Transaction>;

  pollForTxConfirmation: (
    txid: string,
    pendingStatusCb?: (tx: Transaction) => void
  ) => Promise<Transaction>;

  getAccounts: () => Promise<BankAccount[]>;

  getAccount: (accountId: number) => Promise<BankAccount>;

  getAccountBalance: (accountId: number) => Promise<BankAccountBalance>;

  getBanks: () => Promise<Bank[]>;

  transferToReserve: ({
    accountId,
    amountInMinorUnit,
    nonce
  }: APIWrapperReserveTransfer) => Promise<TransferReceipt>;
}

// API Wrapper interfaces

interface APIWrapperTransfer {
  toAddress: string;
  amountInMinorUnit: number;
}

interface APIWrapperCreationRequest {
  accountId: number;
  amountInMinorUnit: number;
}

interface APIWrapperRedeem {
  accountId: number;
  amountInMinorUnit: number;
}

interface APIWrapperHistoryRequest {
  pageSize: number;
  pageNumber: number;
}

interface APIWrapperReserveTransfer {
  accountId: number;
  amountInMinorUnit: number;
  nonce: string;
}

export {
  APIWrapperTransfer,
  APIWrapperCreationRequest,
  APIWrapperRedeem,
  APIWrapperHistoryRequest,
  APIWrapperReserveTransfer,
  IMemberApiService,
  Receipt,
  TransferReceipt
};
