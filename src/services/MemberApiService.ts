import {
  AccountsApi,
  Payment,
  Configuration,
  TransactionsApi,
  MemberApi,
  Transaction,
  TransactionState,
  BankAccount,
  BanksApi,
  Bank
} from "xand-member-api-client";
import { AxiosError } from "axios";

import __env from "@/env";
import {
  APIWrapperCreationRequest,
  APIWrapperHistoryRequest,
  IMemberApiService,
  APIWrapperRedeem,
  APIWrapperReserveTransfer,
  APIWrapperTransfer
} from "./IMemberApiService";
import pollForCondition from "@/lib/pollForCondition";
import appSettings from "@/constants/appSettings";
import MemberApiConnection, {
  Headers as MemberApiHeaders
} from "@/lib/MemberApiConnection";

export default class MemberApiService implements IMemberApiService {
  private accounts: AccountsApi = new AccountsApi();
  private banks: BanksApi = new BanksApi();
  private transactions: TransactionsApi = new TransactionsApi();
  private member: MemberApi = new MemberApi();

  private headers: MemberApiHeaders = {};
  private memberApiConnection: MemberApiConnection;

  constructor() {
    this.memberApiConnection = new MemberApiConnection(__env.MEMBER_API_URL);
    this.configureApi();
  }

  private configureApi() {
    this.headers = this.memberApiConnection.getAuthHeader();
    const apiConfig = new Configuration({
      basePath: this.memberApiConnection.getMemberApiUrl()
    });

    this.accounts = new AccountsApi(apiConfig);
    this.transactions = new TransactionsApi(apiConfig);
    this.member = new MemberApi(apiConfig);
    this.banks = new BanksApi(apiConfig);
  }

  public updateUrl(url: string) {
    this.memberApiConnection.baseUrl = url;
    this.configureApi();
  }

  public updateJwt(token: string | undefined) {
    this.memberApiConnection.jwtToken = token;
    this.configureApi();
  }

  // Xand
  public async getMember() {
    return (await this.member.getMember({ headers: this.headers })).data;
  }

  public async getXANDBalance() {
    return (await this.member.getXANDBalance({ headers: this.headers })).data;
  }

  public async transfer({ toAddress, amountInMinorUnit }: APIWrapperTransfer) {
    const payment: Payment = { toAddress, amountInMinorUnit };
    return (await this.member.sendPayment(payment, { headers: this.headers }))
      .data;
  }

  public async createXAND({
    accountId,
    amountInMinorUnit
  }: APIWrapperCreationRequest) {
    return (
      await this.member.createXAND(
        {
          accountId,
          amountInMinorUnit
        },
        { headers: this.headers }
      )
    ).data;
  }

  public async redeem({ accountId, amountInMinorUnit }: APIWrapperRedeem) {
    return (
      await this.member.redeemXAND(
        {
          accountId,
          amountInMinorUnit
        },
        { headers: this.headers }
      )
    ).data;
  }

  public async getHistory({ pageSize, pageNumber }: APIWrapperHistoryRequest) {
    return (
      await this.member.getTransactionHistory(pageSize, pageNumber, {
        headers: this.headers
      })
    ).data;
  }

  public async getTransaction(transactionId: string) {
    return (
      await this.transactions.getTransactionById(transactionId, {
        headers: this.headers
      })
    ).data;
  }

  public async pollForTxConfirmation(
    txId: string,
    pendingStatusCb?: (tx: Transaction) => void
  ) {
    let pollItem: Transaction | null = null;
    await pollForCondition(
      async () => {
        try {
          pollItem = await this.getTransaction(txId);
        } catch (err) {
          const axiosErr = err as AxiosError;
          if (
            !axiosErr.isAxiosError ||
            !axiosErr.response ||
            !axiosErr.response.status ||
            axiosErr.response.status !== 404
          ) {
            throw err;
          }
        }
      },
      () => {
        if (!pollItem) {
          return false;
        } else if (pollItem.status.state === TransactionState.Pending) {
          if (pendingStatusCb) {
            pendingStatusCb(pollItem);
          }
          return false;
        }
        return true;
      },
      appSettings.MAX_RETRIES
    );
    if (!pollItem) {
      // This should literally never happen because the pollForCondition function will throw
      // if it does not eventually receive a pollItem.
      throw new Error(`No send tx found with id ${txId}.`);
    }
    return pollItem;
  }

  // ACCOUNTS
  public async getAccounts() {
    return (await this.accounts.getAccounts({ headers: this.headers })).data;
  }

  public async getAccount(accountId: number): Promise<BankAccount> {
    return (
      await this.accounts.getAccount(accountId, { headers: this.headers })
    ).data;
  }

  public async getAccountBalance(accountId: number) {
    return (
      await this.accounts.getBalance(accountId, {
        headers: this.headers
      })
    ).data;
  }

  // BANKS
  public async getBanks() {
    return (await this.banks.getBanks({ headers: this.headers })).data;
  }

  public async getBank(bankId: number): Promise<Bank> {
    return (await this.banks.getBank(bankId, { headers: this.headers })).data;
  }

  public async transferToReserve({
    accountId,
    amountInMinorUnit,
    nonce
  }: APIWrapperReserveTransfer) {
    return (
      await this.accounts.submitTransferToReserve(
        accountId,
        {
          amountInMinorUnit,
          nonce
        },
        { headers: this.headers }
      )
    ).data;
  }
}
