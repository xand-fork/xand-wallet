import Vue from "vue";
import Component from "vue-class-component";
import { getModule } from "vuex-module-decorators";

import BankStore from "@/store/modules/bank";
import UserStore from "@/store/modules/user";
import XandStore from "@/store/modules/xand";
import ContactStore from "@/store/modules/contact";
import ModalStore from "@/store/modules/modal";
import SettingsStore from "@/store/modules/settings";

@Component
export default class StoreModules extends Vue {
  get settingsModule() {
    return getModule(SettingsStore, this.$store);
  }
  get bankModule() {
    return getModule(BankStore, this.$store);
  }
  get userModule() {
    return getModule(UserStore, this.$store);
  }
  get xandModule() {
    return getModule(XandStore, this.$store);
  }
  get contactsModule() {
    return getModule(ContactStore, this.$store);
  }
  get modalModule() {
    return getModule(ModalStore, this.$store);
  }
}
