import Vue from "vue";
import Component from "vue-class-component";
import { loggerIndex } from "@/constants/logger";

const clipboardTimeout = 3000;

@Component
export default class CopyToClipboard extends Vue {
  public copyToClipboard(
    el: EventTarget | null,
    alertMsg: string,
    successLogMsg: string,
    errLogMsg: string
  ) {
    if (!el || el === null || !(el instanceof HTMLInputElement)) {
      this.$remoteLogger.error(loggerIndex.ERR_.SER0037 + errLogMsg);
      return false;
    }
    el.select();
    if (window.document.execCommand("copy")) {
      this.$remoteLogger.info(
        loggerIndex.INFO_.SER0013 + successLogMsg + el.value
      );
      this.$sweetAlert("success", alertMsg, clipboardTimeout);
      return true;
    }
    this.$remoteLogger.error(loggerIndex.ERR_.SER0037 + errLogMsg);
    return false;
  }
}
