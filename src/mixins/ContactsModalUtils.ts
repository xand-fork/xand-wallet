import Component, { mixins } from "vue-class-component";

import StoreModules from "@/mixins/StoreModules";
import Contact from "@/models/Contact";
import displayMessages from "@/constants/displayMessages";
import appSettings from "@/constants/appSettings";

@Component
export default class ContactsModalUtils extends mixins(StoreModules) {
  public close() {
    this.$emit("close");
  }

  public sleep = (time: number) => (resolve: (v: any) => void) =>
    setTimeout(resolve, time);

  public transferAmountTo(address: string) {
    this.xandModule.setRecipient(address);
    if (this.$router.currentRoute.path !== "/send") {
      this.$router.replace("send");
    }
    this.modalModule.closeModal();
  }

  public validateAndPrepareContactData(
    contactName?: string,
    company?: string,
    address?: string
  ): Contact {
    if (!company || !address) {
      throw displayMessages.MANDATORY_FIELD;
    }
    if (address && address.length !== appSettings.VALID_ADDRESS_LENGTH) {
      throw displayMessages.INVALID_ADDRESS;
    }
    return { contactName, company, address };
  }
}
