module.exports = {
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        appId: "com.transparentinc.wallet",
        artifactName: "${productName}-${os}-${version}.${ext}",
        mac: {
          category: "public.app-category.finance",
          type: "distribution",
          target: "zip"
        },
        linux: {
          category: "Finance",
          target: "zip"
        },
        win: {
          target: "portable"
        }
      }
    }
  },
  chainWebpack: config => {
    // allow SVG
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule
        .use('svg-url-loader')
        .loader('svg-url-loader');

    if (process.env.NODE_ENV === 'coverage') {
      const tsRule = config.module.rule('ts');

      tsRule
        .use('istanbul')
        .loader('istanbul-instrumenter-loader')
        .options({ esModules: true })
        .before('cache-loader');

      const tsxRule = config.module.rule('tsx');

      tsxRule
        .use('istanbul')
        .loader('istanbul-instrumenter-loader')
        .options({ esModules: true })
        .before('cache-loader');

      config.output
        .devtoolModuleFilenameTemplate('[absolute-resource-path]')
        .devtoolFallbackModuleFilenameTemplate('[absolute-resource-path]?[hash]');
    }
  },
  configureWebpack: {
    devtool: 'inline-cheap-module-source-map',
    resolve: {
      extensions: [".js", ".ts", ".tsx", ".jsx", ".vue", ".html"]
    },
  },
  runtimeCompiler: true,
  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "@/assets/style/_variables.scss";`
      },
    }
  }
}
